import Foundation

class Person{
    
    var name: String
    var id: Int
    var birthDate: Date?
    var Country: String?
  
    init(name: String, id: Int){
        self.name = name
        self.id = id
    }
   
}

class Artist: Person{
    var actedIn: [Movie] = []
    var directed: [Movie] = []
}
