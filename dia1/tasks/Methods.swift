extension Artist{
  func getMoviesByActor(_ actor: Artist) -> [Movie]{
    return actor.actedIn
  }
  func getMoviesByDirector(_ director: Artist) -> [Movie]{
    return director.directed
  }
}

extension Array where Element: Movie{
  
  func orderMoviesByLaunchDate() -> [Movie]{
    return self.sorted(by:{ $0.launchDate < $1.launchDate })
  }
  
  func orderMoviesByName() -> [Movie]{
    return self.sorted(by: { $0.name < $1.name })
  }
    
  func orderMoviesByCategory() -> [Movie]{
    return self.sorted(by: { 
        if let element1 = $0.category{
            if let element2 = $1.category{
                return element1.rawValue < element2.rawValue 
            }
            return true
        }
        return false
    })
      
  }
  
  func getMoviesByCategory(category: Movie.Genre) -> [Movie]{
    return self.filter(
        {(movie: Movie) -> Bool in
            return movie.category == category
        }
    )
  }
    
}
