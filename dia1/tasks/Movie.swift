import Foundation

class Movie{
  
  enum Genre: String{
    case Horror = "Horror"
    case Comedy = "Comedy"
    case Drama = "Drama"
  }
   
  var name: String
  var description: String
  var launchDate: Date
  var category: Genre?
  var rating: Float?
  var budget: Double?
  var director: Artist?
  var Actors: [Artist] = []
   
  init(name: String, description: String, launchDate: Date){
    self.name = name
    self.description = description
    self.launchDate = launchDate
  }
   
 }

