//Two Sum
class Solution {
    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        var result = [0, 1]
        for index1 in 0..<nums.count{
            for index2 in index1+1..<nums.count{
                if(nums[index1] + nums[index2] == target){
                    result[0] = index1
                    result[1] = index2
                    return result
                }
            }
        }
        
        return result
    }
}



//Add two numbers

public class ListNode {
    public var val: Int
    public var next: ListNode?
    
    public init(_ val: Int) {
         self.val = val
         self.next = nil
     }
}
 

class Solution {
    
    func calculate(l1: ListNode?, l2: ListNode?, target: ListNode?){
   
        let sum = getValue(l1) + getValue(l2) + getValue(target)
        target!.val = (sum)%10    
    
        if hasNext(l1) || hasNext(l2){
            target!.next = ListNode(sum/10)
            
            calculate(l1: getNext(l1), l2: getNext(l2), target: getNext(target))
            return
    
        } 
        
        if sum > 9 {
            target!.next = ListNode(sum/10)
        }
            return
    
    } 
    
    func hasNext(_ node: ListNode?) -> Bool{
        if let node1=node, let next=node1.next{
            return true
        }
        
        return false
    }
    
    func addTwoNumbers(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
        
        let result = ListNode(0)
        
        calculate(l1: l1, l2: l2, target: result)
        
        return result as ListNode?
    }
    
    func getValue(_ node: ListNode?) -> Int {
        if let node = node {
            return node.val
        }
        
        return 0
    }
    
    func getNext(_ node: ListNode?) -> ListNode?{
        if let node1=node, let next = node1.next{
            return next
        }
        
        return nil
    }
}


