class Solution {
    
    func wordBreak(_ s: String, _ wordDict: [String]) -> Bool {
        var searched : [String : Bool] = [:]
        var dict = wordDict
        
        return wordSearch(s, &dict, searched: &searched)
    
    }
    
    func updateDictionary(dict: inout [String : Bool], key: String, value: Bool) -> Bool{
        dict[key] = value
        return value
    }
    
    
    func wordSearch(_ s: String, _ wordDict: inout [String], searched: inout [String : Bool]) -> Bool {
        for word in wordDict{
            
            if searched[s] != nil{
                return updateDictionary(dict: &searched, key: s, value: false)}
            
            if s.isEmpty{
                return updateDictionary(dict: &searched, key: s, value: false)}
            
            if let range = s.range(of: word){
                
                if s == word {
                    searched[s] = true
                    return true}
                
                let prefix = s.prefix(upTo: range.lowerBound)
                
                var suffix : Substring
                
                suffix = s.suffix(from: range.upperBound)
                
                if !prefix.isEmpty && suffix.isEmpty{
                    
                    if wordSearch(String(prefix), &wordDict, searched: &searched) {
                        return updateDictionary(dict: &searched, key: s, value: true)
                    }
                }
                
                else if prefix.isEmpty && !suffix.isEmpty{
                    if wordSearch(String(suffix), &wordDict, searched: &searched) {
                        return updateDictionary(dict: &searched, key: s, value: true)
                    }
                } 
                
                else {
                    if wordSearch(String(prefix), &wordDict, searched: &searched) && wordSearch(String(suffix), &wordDict, searched: &searched) {
                        return updateDictionary(dict: &searched, key: s, value: true)
                    }
                } 
            }
            
        }
        return updateDictionary(dict: &searched, key: s, value: false)     
    }

}
