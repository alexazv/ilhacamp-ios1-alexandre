class Solution {
       
    func reconstructQueue(_ people: [[Int]]) -> [[Int]] {
        
        var sorted = people.sorted(by: {
            if $0[0] != $1[0]{
                return $1[0] <= $0[0]
             }
                return $0[1] <= $1[1]
            })
        
        var result : [[Int]] = []
        
        for person in sorted {
            result.insert(person, at: person[1])
        }
        
        
        return result
    }
}

