/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public var val: Int
 *     public var left: TreeNode?
 *     public var right: TreeNode?
 *     public init(_ val: Int) {
 *         self.val = val
 *         self.left = nil
 *         self.right = nil
 *     }
 * }
 */
class Solution {
    func invertTree(_ root: TreeNode?) -> TreeNode? {
        if let node = root{
            let rightNode = node.right
        
            node.right = node.left
            node.left = rightNode
        
            invertTree(node.left)
            invertTree(node.right)
            return node
        }
        return root
    }
}
