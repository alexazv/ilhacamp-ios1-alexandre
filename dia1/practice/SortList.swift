/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public var val: Int
 *     public var next: ListNode?
 *     public init(_ val: Int) {
 *         self.val = val
 *         self.next = nil
 *     }
 * }
 */
class Solution {
    func sortList(_ head: ListNode?) -> ListNode? {
        
        if(head == nil){return head} 
        
        var map : [ListNode] = []
        
        var length = 1
        var current = head!
        map.append(current)
        
        while(current.next != nil){     
            current = current.next!
            length+=1
            map.append(current)
        }
        
        quickSort(head, lo: 0, hi: length - 1, map: &map)
        return head
    }
    
    func swap(_ first: ListNode, _ second: ListNode){
        let value = first.val
        first.val = second.val
        second.val = value
    }
    
    func quickSort(_ head: ListNode?, lo: Int, hi: Int, map: inout [ListNode]){
        if lo < hi{

            let indices = partition(head, pivot: map[hi].val, lo: lo, hi: hi, map: &map)
            quickSort(head, lo: lo, hi: indices.left-1, map: &map)
            quickSort(head, lo: indices.right+1, hi: hi, map: &map)
        }
    }
       
    func partition(_ head: ListNode?, pivot: Int, lo: Int, hi: Int, map: inout [ListNode]) -> (left: Int, right: Int){
           
        var left = lo
        var right = lo
        var n = hi
        
        while right <= n{
            if map[right].val < pivot{
                swap(map[left], map[right])
                left += 1
                right += 1
            }
            else if map[right].val > pivot{
                swap(map[right], map[n])
                n -= 1
            }
            else{
                right+=1
            }
        }
        
        return (left: left, n)
    }
}
