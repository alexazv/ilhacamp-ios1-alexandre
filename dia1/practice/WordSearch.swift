import Foundation

class Solution {
    func exist(_ board: [[Character]], _ word: String) -> Bool {
        
        for i in 0..<board.count{
            for j in 0..<board[i].count{
                    if wordSearch(board, word, letterPosition: 0, 
                                  xStart: i, yStart: j, visited: []){
                        return true
                    }
                }                               
            }
        
        return false
                
        }

    func wordSearch(_ board: [[Character]], _ word: String, letterPosition: Int,
                    xStart: Int, yStart: Int, visited: [(Int, Int)]) -> Bool{
        for x in -1...1{
            for y in -1...1{
                if (abs(x) + abs(y) <= 1) && (xStart+x >= 0 && yStart+y>=0) && 
                (xStart+x < board.count && yStart+y < board[xStart+x].count) &&
                (!visited.contains((xStart+x, yStart+y))) && 
                (board[xStart+x][yStart+y] == word[word.index(word.startIndex, offsetBy: letterPosition)]){
                    if letterPosition == word.count-1{
                        return true
                    }
                    
                    var newVisited = visited
                    newVisited.append((xStart+x, yStart+y))
                    if wordSearch(board, word, letterPosition: letterPosition+1, 
                                      xStart: xStart+x, yStart: yStart+y, 
                                      visited: newVisited){
                        return true
                    }
                }
            }
        }  
        return false
    }
}

typealias IntTuple = (Int, Int) 
    
extension Sequence where Iterator.Element == IntTuple{
    func contains(_ target: (Int, Int)) -> Bool{
        for element in self{
            if target.0 == element.0 && target.1 == element.1{
                return true
            }
        }
        return false
    }
}
