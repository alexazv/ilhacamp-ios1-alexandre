class Solution {
    
    func topKFrequent(_ nums: [Int], _ k: Int) -> [Int] {
        var map = [Int: Int]()
        var ret : [Int] = []
        
        for i in 0..<nums.count{
            
            if let count = map[nums[i]]{
                map[nums[i]] = count+1
            }
            else {
                map[nums[i]] = 1
            }
        }
        
        let sortedMap = map.sorted(by: {$0.value > $1.value})
        for i in 0..<k{
            ret.append(sortedMap[i].key)
        }
        
        return ret
    }
}
