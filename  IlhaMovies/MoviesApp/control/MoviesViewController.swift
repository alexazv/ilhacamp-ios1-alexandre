//
//  MoviesViewController.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 28/04/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var movies : [Movie] = []
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 110, height: 174)
        layout.minimumLineSpacing = 5
        
        collectionView.collectionViewLayout = layout
        collectionView.dataSource = self
        
        collectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MovieCollectionViewCell")
        
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        
        navigationItem.hidesSearchBarWhenScrolling = false
        
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        
        if let navigation = self.navigationController {
            navigation.navigationBar.barTintColor = UIColor.white
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionViewCell", for: indexPath) as! MovieCollectionViewCell
        
        if(indexPath.item >= movies.count) {
            
            cell.setupCell(url: "")
        }
        else {
            
            //let title = movies[indexPath.item].title!
            let posterURL = movies[indexPath.item].thumbnailUrl!
            
            cell.setupCell(url: posterURL)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let singleMovieView = SingleMovieViewController(id: movies[indexPath.item].imdbId)
        
        let nav = UINavigationController(rootViewController: singleMovieView)
        present(nav, animated: true, completion: nil)
    }
}

extension MoviesViewController: UISearchResultsUpdating {
    
    /*func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        if let text = searchBar.text, !text.isEmpty{
            RequestMaker.getMoviesByName(name: text)
            { (movies : [Movie]) in
                self.movies = movies
            }
        }
        collectionView.reloadData()
    }*/
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if let text = searchController.searchBar.text, !text.isEmpty{
            RequestMaker.getMoviesByName(name: text)
            { (movies : [Movie]) in
                self.movies = movies
            }
        }
        
        collectionView.reloadData()
    }
}
