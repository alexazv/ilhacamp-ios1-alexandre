//
//  MovieCollectionViewCell.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 28/04/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var moviePoster: UIImageView!
    
    @IBOutlet weak var cellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /*func setupCell(color: UIColor, number: Int){
        self.backgroundColor = color
        cellLabel.text = String(number)
    }
    
    func setupCell(color: UIColor, label: String){
        self.backgroundColor = color
        cellLabel.text = label
    }*/
    
    func setupCell(url : String){
        if url.isEmpty{
            if let placeholder = UIImage(named: "place_hold"){
                moviePoster.image = placeholder
            }
            return
        }
        
        moviePoster.sd_setImage(with: URL(string: url), completed: nil)
    }
}
