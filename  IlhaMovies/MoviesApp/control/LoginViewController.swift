//
//  LoginViewController.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 05/05/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var btStart: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        btStart.layer.cornerRadius = btStart.frame.height/2
        btStart.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
