//
//  SingleMovieViewController.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 05/05/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import UIKit

class SingleMovieViewController: UIViewController {
    
    var id: String?
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var plotText: UILabel!
    
    init(id: String?){
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder decoder: NSCoder){
        super.init(coder: decoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RequestMaker.getMovieById(id: id!, completion: {(movieResponse: Movie?) in
            
            if let movie = movieResponse{
                self.moviePoster.sd_setImage(with: URL(string: movie.thumbnailUrl!), completed: nil)
                self.movieTitle.text = movie.title! + "(" + movie.year! + ")"
                self.plotText.text = movie.plot!
            }
        })
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
