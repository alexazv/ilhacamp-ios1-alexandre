//
//  RequestMaker.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 28/04/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SDWebImage

class OMDBResult: Mappable {
    var movies: [Movie] = []
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        movies <- map["Search"]
    }
}

class RequestMaker {
    
    static let apikey = "3579c871"
    
    static func getMoviesByName(name: String, completion: @escaping ([Movie]) -> Void) {
        
        let parameters : Parameters = [
            "apikey" : apikey,
            "s": name
        ]
        
        let request = Alamofire.request(
            "http://www.omdbapi.com",
            parameters: parameters
        )

        request.responseObject(completionHandler: { (responseWrapper: DataResponse<OMDBResult>) in
            if let movies = responseWrapper.value?.movies {
                completion(movies)
                return
            }
            completion([])
        })
    }
    
    static func getMovieById(id: String, completion: @escaping (Movie?) -> Void) {
        
        let parameters : Parameters = [
            "apikey" : apikey,
            "i": id
        ]
        
        let request = Alamofire.request(
            "http://www.omdbapi.com",
            parameters: parameters
        )
        
        request.responseObject(completionHandler: { (responseWrapper: DataResponse<Movie>) in
            if let movie = responseWrapper.value {
                completion(movie)   
                return
            }
            completion(nil)
        })
    }
}
