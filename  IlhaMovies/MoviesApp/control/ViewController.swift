//
//  ViewController.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 21/04/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var btStart: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btStart.layer.cornerRadius = btStart.frame.height/2
        btStart.clipsToBounds = true
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can 	be recreated.
    }

    @IBAction func btStartClicked(_ sender: Any) {
        let moviesView = MoviesViewController()
        
        let nav = UINavigationController(rootViewController: moviesView)
        present(nav, animated: true, completion: nil)
    }
}

