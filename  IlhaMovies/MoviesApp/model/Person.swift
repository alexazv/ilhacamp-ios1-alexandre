//
//  Person.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 28/04/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import Foundation

class Person{
    
    var name: String
    var id: Int
    var birthDate: Date?
    var Country: String?
    
    init(name: String, id: Int){
        self.name = name
        self.id = id
    }
    
}

class Artist: Person{
    var actedIn: [Movie] = []
    var directed: [Movie] = []
}
