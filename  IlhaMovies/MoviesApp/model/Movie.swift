//
//  Movie.swift
//  MoviesApp
//
//  Created by ilhacamp5 on 28/04/2018.
//  Copyright © 2018 ilhacamp5. All rights reserved.
//

import Foundation
import ObjectMapper

class Movie : Mappable {
    
    enum Genre: String {
        case Horror = "Horror"
        case Comedy = "Comedy"
        case Drama = "Drama"
    }
    
    var imdbId: String?
    var title: String?
    var plot: String?
    var year: String?
    var duration : TimeInterval?
    var category: Genre?
    var imdbRating: Float?
    var thumbnailUrl : String?
    //var director: Artist?
    //var Actors: [Artist] = []
    
    //Mappable Functions
    func mapping(map: Map){
        imdbId <- map["imdbID"]
        title <- map["Title"]
        year <- map["Year"]
        plot <- map["Plot"]
        thumbnailUrl <- map["Poster"]
    }
    
    required init?(map: Map) {
    }
    
    /*init(id: Int, title: String){
        self.imdbId = id
        self.title = title
    }*/
}

